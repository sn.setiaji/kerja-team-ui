// import Muc from 'views/muc'
// import View from 'views/view'
// import Component from 'components/tagihan.component'
// import Mitra from 'components/mitra.component'
// import Popup from 'components/popup.component'

var sidebar = {
	init: function(){
		this.dom()
		this.event()
	},
	dom: function(){
		this.$el = $('.menu-togle')
		this.$target = this.$el.attr('menu-target')
	},
	event: function(){
		var el = this.$el
		var target = this.$target
		el.click(function(){
			var menuToShow = $(this).attr('menu-target')
			$('.menu-togle.active').removeClass('active')
			$(this).addClass('active')
			$('.menu-togle--list.show').removeClass('show')
			$(menuToShow).addClass('show')
		})
	}
}
sidebar.init()

var userOption = {
	init: function(){
		this.dom()
		this.event()
	},
	dom: function(){
		this.$el = $('.user-panel')
		this.$target = this.$el.attr('target')
	},
	event: function(){
		var el = this.$el
		var target = this.$target
		var bool = false
		el.click(function(){
			bool = !bool
			if(bool == true){
				$(target).removeClass('fadeOutDown').addClass('fadeInUp')
				$(target).addClass('active')
			} else {
				$(target).removeClass('fadeInUp').addClass('fadeOutDown')
				setTimeout(function(){
					$(target).removeClass('active')
				}, 500)
			}
		})
	}
}
userOption.init()

var hiddenNav = {
	init: function(){
		this.dom()
		this.event()
	},
	dom: function(){
		this.$el = $('.hidden-nav-toggle')
		this.$target = $('.hidden-nav')
	},
	event: function(){
		var el = this.$el
		var target = this.$target
		el.click(function(){
			target.toggleClass('active');
			$(this).toggleClass('rotated')
		})
	}
}
hiddenNav.init()

var modalScript = {
	init: function(){
		this.dom()
		this.event()
		this.close()
	},
	dom: function(){
		this.$el = $('.modal-togle')
		this.$closeModal = $('.close-modal')
		this.$closeModalBG = $('.se-modal--close')
		this.$closeModalTarget = $('.se-modal')
	},
	event: function(){
		var el = this.$el
		el.click(function(){
			var target = $(this).attr('modal-target')
			$(target).show()
		})
	},
	close: function(){
		var close = this.$closeModal
		var closeBG = this.$closeModalBG
		var targetModal = this.$closeModalTarget
		close.click(function(e){
			targetModal.hide()
		})
		closeBG.click(function(e){
			targetModal.hide()
		})
	}
}
modalScript.init()
