(function(){function r(e,n,t){function o(i,f){if(!n[i]){if(!e[i]){var c="function"==typeof require&&require;if(!f&&c)return c(i,!0);if(u)return u(i,!0);var a=new Error("Cannot find module '"+i+"'");throw a.code="MODULE_NOT_FOUND",a}var p=n[i]={exports:{}};e[i][0].call(p.exports,function(r){var n=e[i][1][r];return o(n||r)},p,p.exports,r,e,n,t)}return n[i].exports}for(var u="function"==typeof require&&require,i=0;i<t.length;i++)o(t[i]);return o}return r})()({"E:\\Document\\Project(s)\\kerjateam-ui\\src\\src\\scripts\\main.js":[function(require,module,exports){
'use strict';

// import Muc from 'views/muc'
// import View from 'views/view'
// import Component from 'components/tagihan.component'
// import Mitra from 'components/mitra.component'
// import Popup from 'components/popup.component'

var sidebar = {
	init: function init() {
		this.dom();
		this.event();
	},
	dom: function dom() {
		this.$el = $('.menu-togle');
		this.$target = this.$el.attr('menu-target');
	},
	event: function event() {
		var el = this.$el;
		var target = this.$target;
		el.click(function () {
			var menuToShow = $(this).attr('menu-target');
			$('.menu-togle.active').removeClass('active');
			$(this).addClass('active');
			$('.menu-togle--list.show').removeClass('show');
			$(menuToShow).addClass('show');
		});
	}
};
sidebar.init();

var userOption = {
	init: function init() {
		this.dom();
		this.event();
	},
	dom: function dom() {
		this.$el = $('.user-panel');
		this.$target = this.$el.attr('target');
	},
	event: function event() {
		var el = this.$el;
		var target = this.$target;
		var bool = false;
		el.click(function () {
			bool = !bool;
			if (bool == true) {
				$(target).removeClass('fadeOutDown').addClass('fadeInUp');
				$(target).addClass('active');
			} else {
				$(target).removeClass('fadeInUp').addClass('fadeOutDown');
				setTimeout(function () {
					$(target).removeClass('active');
				}, 500);
			}
		});
	}
};
userOption.init();

var hiddenNav = {
	init: function init() {
		this.dom();
		this.event();
	},
	dom: function dom() {
		this.$el = $('.hidden-nav-toggle');
		this.$target = $('.hidden-nav');
	},
	event: function event() {
		var el = this.$el;
		var target = this.$target;
		el.click(function () {
			target.toggleClass('active');
			$(this).toggleClass('rotated');
		});
	}
};
hiddenNav.init();

var modalScript = {
	init: function init() {
		this.dom();
		this.event();
		this.close();
	},
	dom: function dom() {
		this.$el = $('.modal-togle');
		this.$closeModal = $('.close-modal');
		this.$closeModalBG = $('.se-modal--close');
		this.$closeModalTarget = $('.se-modal');
	},
	event: function event() {
		var el = this.$el;
		el.click(function () {
			var target = $(this).attr('modal-target');
			$(target).show();
		});
	},
	close: function close() {
		var close = this.$closeModal;
		var closeBG = this.$closeModalBG;
		var targetModal = this.$closeModalTarget;
		close.click(function (e) {
			targetModal.hide();
		});
		closeBG.click(function (e) {
			targetModal.hide();
		});
	}
};
modalScript.init();

},{}]},{},["E:\\Document\\Project(s)\\kerjateam-ui\\src\\src\\scripts\\main.js"])
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm5vZGVfbW9kdWxlcy9icm93c2VyLXBhY2svX3ByZWx1ZGUuanMiLCJzcmMvc2NyaXB0cy9tYWluLmpzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBOzs7QUNBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBLElBQUksVUFBVTtBQUNiLE9BQU0sU0FBQSxJQUFBLEdBQVU7QUFDZixPQUFBLEdBQUE7QUFDQSxPQUFBLEtBQUE7QUFIWSxFQUFBO0FBS2IsTUFBSyxTQUFBLEdBQUEsR0FBVTtBQUNkLE9BQUEsR0FBQSxHQUFXLEVBQVgsYUFBVyxDQUFYO0FBQ0EsT0FBQSxPQUFBLEdBQWUsS0FBQSxHQUFBLENBQUEsSUFBQSxDQUFmLGFBQWUsQ0FBZjtBQVBZLEVBQUE7QUFTYixRQUFPLFNBQUEsS0FBQSxHQUFVO0FBQ2hCLE1BQUksS0FBSyxLQUFULEdBQUE7QUFDQSxNQUFJLFNBQVMsS0FBYixPQUFBO0FBQ0EsS0FBQSxLQUFBLENBQVMsWUFBVTtBQUNsQixPQUFJLGFBQWEsRUFBQSxJQUFBLEVBQUEsSUFBQSxDQUFqQixhQUFpQixDQUFqQjtBQUNBLEtBQUEsb0JBQUEsRUFBQSxXQUFBLENBQUEsUUFBQTtBQUNBLEtBQUEsSUFBQSxFQUFBLFFBQUEsQ0FBQSxRQUFBO0FBQ0EsS0FBQSx3QkFBQSxFQUFBLFdBQUEsQ0FBQSxNQUFBO0FBQ0EsS0FBQSxVQUFBLEVBQUEsUUFBQSxDQUFBLE1BQUE7QUFMRCxHQUFBO0FBT0E7QUFuQlksQ0FBZDtBQXFCQSxRQUFBLElBQUE7O0FBRUEsSUFBSSxhQUFhO0FBQ2hCLE9BQU0sU0FBQSxJQUFBLEdBQVU7QUFDZixPQUFBLEdBQUE7QUFDQSxPQUFBLEtBQUE7QUFIZSxFQUFBO0FBS2hCLE1BQUssU0FBQSxHQUFBLEdBQVU7QUFDZCxPQUFBLEdBQUEsR0FBVyxFQUFYLGFBQVcsQ0FBWDtBQUNBLE9BQUEsT0FBQSxHQUFlLEtBQUEsR0FBQSxDQUFBLElBQUEsQ0FBZixRQUFlLENBQWY7QUFQZSxFQUFBO0FBU2hCLFFBQU8sU0FBQSxLQUFBLEdBQVU7QUFDaEIsTUFBSSxLQUFLLEtBQVQsR0FBQTtBQUNBLE1BQUksU0FBUyxLQUFiLE9BQUE7QUFDQSxNQUFJLE9BQUosS0FBQTtBQUNBLEtBQUEsS0FBQSxDQUFTLFlBQVU7QUFDbEIsVUFBTyxDQUFQLElBQUE7QUFDQSxPQUFHLFFBQUgsSUFBQSxFQUFnQjtBQUNmLE1BQUEsTUFBQSxFQUFBLFdBQUEsQ0FBQSxhQUFBLEVBQUEsUUFBQSxDQUFBLFVBQUE7QUFDQSxNQUFBLE1BQUEsRUFBQSxRQUFBLENBQUEsUUFBQTtBQUZELElBQUEsTUFHTztBQUNOLE1BQUEsTUFBQSxFQUFBLFdBQUEsQ0FBQSxVQUFBLEVBQUEsUUFBQSxDQUFBLGFBQUE7QUFDQSxlQUFXLFlBQVU7QUFDcEIsT0FBQSxNQUFBLEVBQUEsV0FBQSxDQUFBLFFBQUE7QUFERCxLQUFBLEVBQUEsR0FBQTtBQUdBO0FBVkYsR0FBQTtBQVlBO0FBekJlLENBQWpCO0FBMkJBLFdBQUEsSUFBQTs7QUFFQSxJQUFJLFlBQVk7QUFDZixPQUFNLFNBQUEsSUFBQSxHQUFVO0FBQ2YsT0FBQSxHQUFBO0FBQ0EsT0FBQSxLQUFBO0FBSGMsRUFBQTtBQUtmLE1BQUssU0FBQSxHQUFBLEdBQVU7QUFDZCxPQUFBLEdBQUEsR0FBVyxFQUFYLG9CQUFXLENBQVg7QUFDQSxPQUFBLE9BQUEsR0FBZSxFQUFmLGFBQWUsQ0FBZjtBQVBjLEVBQUE7QUFTZixRQUFPLFNBQUEsS0FBQSxHQUFVO0FBQ2hCLE1BQUksS0FBSyxLQUFULEdBQUE7QUFDQSxNQUFJLFNBQVMsS0FBYixPQUFBO0FBQ0EsS0FBQSxLQUFBLENBQVMsWUFBVTtBQUNsQixVQUFBLFdBQUEsQ0FBQSxRQUFBO0FBQ0EsS0FBQSxJQUFBLEVBQUEsV0FBQSxDQUFBLFNBQUE7QUFGRCxHQUFBO0FBSUE7QUFoQmMsQ0FBaEI7QUFrQkEsVUFBQSxJQUFBOztBQUVBLElBQUksY0FBYztBQUNqQixPQUFNLFNBQUEsSUFBQSxHQUFVO0FBQ2YsT0FBQSxHQUFBO0FBQ0EsT0FBQSxLQUFBO0FBQ0EsT0FBQSxLQUFBO0FBSmdCLEVBQUE7QUFNakIsTUFBSyxTQUFBLEdBQUEsR0FBVTtBQUNkLE9BQUEsR0FBQSxHQUFXLEVBQVgsY0FBVyxDQUFYO0FBQ0EsT0FBQSxXQUFBLEdBQW1CLEVBQW5CLGNBQW1CLENBQW5CO0FBQ0EsT0FBQSxhQUFBLEdBQXFCLEVBQXJCLGtCQUFxQixDQUFyQjtBQUNBLE9BQUEsaUJBQUEsR0FBeUIsRUFBekIsV0FBeUIsQ0FBekI7QUFWZ0IsRUFBQTtBQVlqQixRQUFPLFNBQUEsS0FBQSxHQUFVO0FBQ2hCLE1BQUksS0FBSyxLQUFULEdBQUE7QUFDQSxLQUFBLEtBQUEsQ0FBUyxZQUFVO0FBQ2xCLE9BQUksU0FBUyxFQUFBLElBQUEsRUFBQSxJQUFBLENBQWIsY0FBYSxDQUFiO0FBQ0EsS0FBQSxNQUFBLEVBQUEsSUFBQTtBQUZELEdBQUE7QUFkZ0IsRUFBQTtBQW1CakIsUUFBTyxTQUFBLEtBQUEsR0FBVTtBQUNoQixNQUFJLFFBQVEsS0FBWixXQUFBO0FBQ0EsTUFBSSxVQUFVLEtBQWQsYUFBQTtBQUNBLE1BQUksY0FBYyxLQUFsQixpQkFBQTtBQUNBLFFBQUEsS0FBQSxDQUFZLFVBQUEsQ0FBQSxFQUFXO0FBQ3RCLGVBQUEsSUFBQTtBQURELEdBQUE7QUFHQSxVQUFBLEtBQUEsQ0FBYyxVQUFBLENBQUEsRUFBVztBQUN4QixlQUFBLElBQUE7QUFERCxHQUFBO0FBR0E7QUE3QmdCLENBQWxCO0FBK0JBLFlBQUEsSUFBQSIsImZpbGUiOiJnZW5lcmF0ZWQuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlc0NvbnRlbnQiOlsiKGZ1bmN0aW9uKCl7ZnVuY3Rpb24gcihlLG4sdCl7ZnVuY3Rpb24gbyhpLGYpe2lmKCFuW2ldKXtpZighZVtpXSl7dmFyIGM9XCJmdW5jdGlvblwiPT10eXBlb2YgcmVxdWlyZSYmcmVxdWlyZTtpZighZiYmYylyZXR1cm4gYyhpLCEwKTtpZih1KXJldHVybiB1KGksITApO3ZhciBhPW5ldyBFcnJvcihcIkNhbm5vdCBmaW5kIG1vZHVsZSAnXCIraStcIidcIik7dGhyb3cgYS5jb2RlPVwiTU9EVUxFX05PVF9GT1VORFwiLGF9dmFyIHA9bltpXT17ZXhwb3J0czp7fX07ZVtpXVswXS5jYWxsKHAuZXhwb3J0cyxmdW5jdGlvbihyKXt2YXIgbj1lW2ldWzFdW3JdO3JldHVybiBvKG58fHIpfSxwLHAuZXhwb3J0cyxyLGUsbix0KX1yZXR1cm4gbltpXS5leHBvcnRzfWZvcih2YXIgdT1cImZ1bmN0aW9uXCI9PXR5cGVvZiByZXF1aXJlJiZyZXF1aXJlLGk9MDtpPHQubGVuZ3RoO2krKylvKHRbaV0pO3JldHVybiBvfXJldHVybiByfSkoKSIsIi8vIGltcG9ydCBNdWMgZnJvbSAndmlld3MvbXVjJ1xyXG4vLyBpbXBvcnQgVmlldyBmcm9tICd2aWV3cy92aWV3J1xyXG4vLyBpbXBvcnQgQ29tcG9uZW50IGZyb20gJ2NvbXBvbmVudHMvdGFnaWhhbi5jb21wb25lbnQnXHJcbi8vIGltcG9ydCBNaXRyYSBmcm9tICdjb21wb25lbnRzL21pdHJhLmNvbXBvbmVudCdcclxuLy8gaW1wb3J0IFBvcHVwIGZyb20gJ2NvbXBvbmVudHMvcG9wdXAuY29tcG9uZW50J1xyXG5cclxudmFyIHNpZGViYXIgPSB7XHJcblx0aW5pdDogZnVuY3Rpb24oKXtcclxuXHRcdHRoaXMuZG9tKClcclxuXHRcdHRoaXMuZXZlbnQoKVxyXG5cdH0sXHJcblx0ZG9tOiBmdW5jdGlvbigpe1xyXG5cdFx0dGhpcy4kZWwgPSAkKCcubWVudS10b2dsZScpXHJcblx0XHR0aGlzLiR0YXJnZXQgPSB0aGlzLiRlbC5hdHRyKCdtZW51LXRhcmdldCcpXHJcblx0fSxcclxuXHRldmVudDogZnVuY3Rpb24oKXtcclxuXHRcdHZhciBlbCA9IHRoaXMuJGVsXHJcblx0XHR2YXIgdGFyZ2V0ID0gdGhpcy4kdGFyZ2V0XHJcblx0XHRlbC5jbGljayhmdW5jdGlvbigpe1xyXG5cdFx0XHR2YXIgbWVudVRvU2hvdyA9ICQodGhpcykuYXR0cignbWVudS10YXJnZXQnKVxyXG5cdFx0XHQkKCcubWVudS10b2dsZS5hY3RpdmUnKS5yZW1vdmVDbGFzcygnYWN0aXZlJylcclxuXHRcdFx0JCh0aGlzKS5hZGRDbGFzcygnYWN0aXZlJylcclxuXHRcdFx0JCgnLm1lbnUtdG9nbGUtLWxpc3Quc2hvdycpLnJlbW92ZUNsYXNzKCdzaG93JylcclxuXHRcdFx0JChtZW51VG9TaG93KS5hZGRDbGFzcygnc2hvdycpXHJcblx0XHR9KVxyXG5cdH1cclxufVxyXG5zaWRlYmFyLmluaXQoKVxyXG5cclxudmFyIHVzZXJPcHRpb24gPSB7XHJcblx0aW5pdDogZnVuY3Rpb24oKXtcclxuXHRcdHRoaXMuZG9tKClcclxuXHRcdHRoaXMuZXZlbnQoKVxyXG5cdH0sXHJcblx0ZG9tOiBmdW5jdGlvbigpe1xyXG5cdFx0dGhpcy4kZWwgPSAkKCcudXNlci1wYW5lbCcpXHJcblx0XHR0aGlzLiR0YXJnZXQgPSB0aGlzLiRlbC5hdHRyKCd0YXJnZXQnKVxyXG5cdH0sXHJcblx0ZXZlbnQ6IGZ1bmN0aW9uKCl7XHJcblx0XHR2YXIgZWwgPSB0aGlzLiRlbFxyXG5cdFx0dmFyIHRhcmdldCA9IHRoaXMuJHRhcmdldFxyXG5cdFx0dmFyIGJvb2wgPSBmYWxzZVxyXG5cdFx0ZWwuY2xpY2soZnVuY3Rpb24oKXtcclxuXHRcdFx0Ym9vbCA9ICFib29sXHJcblx0XHRcdGlmKGJvb2wgPT0gdHJ1ZSl7XHJcblx0XHRcdFx0JCh0YXJnZXQpLnJlbW92ZUNsYXNzKCdmYWRlT3V0RG93bicpLmFkZENsYXNzKCdmYWRlSW5VcCcpXHJcblx0XHRcdFx0JCh0YXJnZXQpLmFkZENsYXNzKCdhY3RpdmUnKVxyXG5cdFx0XHR9IGVsc2Uge1xyXG5cdFx0XHRcdCQodGFyZ2V0KS5yZW1vdmVDbGFzcygnZmFkZUluVXAnKS5hZGRDbGFzcygnZmFkZU91dERvd24nKVxyXG5cdFx0XHRcdHNldFRpbWVvdXQoZnVuY3Rpb24oKXtcclxuXHRcdFx0XHRcdCQodGFyZ2V0KS5yZW1vdmVDbGFzcygnYWN0aXZlJylcclxuXHRcdFx0XHR9LCA1MDApXHJcblx0XHRcdH1cclxuXHRcdH0pXHJcblx0fVxyXG59XHJcbnVzZXJPcHRpb24uaW5pdCgpXHJcblxyXG52YXIgaGlkZGVuTmF2ID0ge1xyXG5cdGluaXQ6IGZ1bmN0aW9uKCl7XHJcblx0XHR0aGlzLmRvbSgpXHJcblx0XHR0aGlzLmV2ZW50KClcclxuXHR9LFxyXG5cdGRvbTogZnVuY3Rpb24oKXtcclxuXHRcdHRoaXMuJGVsID0gJCgnLmhpZGRlbi1uYXYtdG9nZ2xlJylcclxuXHRcdHRoaXMuJHRhcmdldCA9ICQoJy5oaWRkZW4tbmF2JylcclxuXHR9LFxyXG5cdGV2ZW50OiBmdW5jdGlvbigpe1xyXG5cdFx0dmFyIGVsID0gdGhpcy4kZWxcclxuXHRcdHZhciB0YXJnZXQgPSB0aGlzLiR0YXJnZXRcclxuXHRcdGVsLmNsaWNrKGZ1bmN0aW9uKCl7XHJcblx0XHRcdHRhcmdldC50b2dnbGVDbGFzcygnYWN0aXZlJyk7XHJcblx0XHRcdCQodGhpcykudG9nZ2xlQ2xhc3MoJ3JvdGF0ZWQnKVxyXG5cdFx0fSlcclxuXHR9XHJcbn1cclxuaGlkZGVuTmF2LmluaXQoKVxyXG5cclxudmFyIG1vZGFsU2NyaXB0ID0ge1xyXG5cdGluaXQ6IGZ1bmN0aW9uKCl7XHJcblx0XHR0aGlzLmRvbSgpXHJcblx0XHR0aGlzLmV2ZW50KClcclxuXHRcdHRoaXMuY2xvc2UoKVxyXG5cdH0sXHJcblx0ZG9tOiBmdW5jdGlvbigpe1xyXG5cdFx0dGhpcy4kZWwgPSAkKCcubW9kYWwtdG9nbGUnKVxyXG5cdFx0dGhpcy4kY2xvc2VNb2RhbCA9ICQoJy5jbG9zZS1tb2RhbCcpXHJcblx0XHR0aGlzLiRjbG9zZU1vZGFsQkcgPSAkKCcuc2UtbW9kYWwtLWNsb3NlJylcclxuXHRcdHRoaXMuJGNsb3NlTW9kYWxUYXJnZXQgPSAkKCcuc2UtbW9kYWwnKVxyXG5cdH0sXHJcblx0ZXZlbnQ6IGZ1bmN0aW9uKCl7XHJcblx0XHR2YXIgZWwgPSB0aGlzLiRlbFxyXG5cdFx0ZWwuY2xpY2soZnVuY3Rpb24oKXtcclxuXHRcdFx0dmFyIHRhcmdldCA9ICQodGhpcykuYXR0cignbW9kYWwtdGFyZ2V0JylcclxuXHRcdFx0JCh0YXJnZXQpLnNob3coKVxyXG5cdFx0fSlcclxuXHR9LFxyXG5cdGNsb3NlOiBmdW5jdGlvbigpe1xyXG5cdFx0dmFyIGNsb3NlID0gdGhpcy4kY2xvc2VNb2RhbFxyXG5cdFx0dmFyIGNsb3NlQkcgPSB0aGlzLiRjbG9zZU1vZGFsQkdcclxuXHRcdHZhciB0YXJnZXRNb2RhbCA9IHRoaXMuJGNsb3NlTW9kYWxUYXJnZXRcclxuXHRcdGNsb3NlLmNsaWNrKGZ1bmN0aW9uKGUpe1xyXG5cdFx0XHR0YXJnZXRNb2RhbC5oaWRlKClcclxuXHRcdH0pXHJcblx0XHRjbG9zZUJHLmNsaWNrKGZ1bmN0aW9uKGUpe1xyXG5cdFx0XHR0YXJnZXRNb2RhbC5oaWRlKClcclxuXHRcdH0pXHJcblx0fVxyXG59XHJcbm1vZGFsU2NyaXB0LmluaXQoKVxyXG4iXX0=
